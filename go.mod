module bitbucket.org/pcasmath/integervector

go 1.16

require (
	bitbucket.org/pcasmath/integer v0.0.1
	bitbucket.org/pcasmath/monomialorder v0.0.1
	bitbucket.org/pcasmath/object v0.0.4
	bitbucket.org/pcasmath/objectmap v0.0.1
	bitbucket.org/pcasmath/slice v0.0.4
	bitbucket.org/pcastools/gobutil v1.0.3
	bitbucket.org/pcastools/stringsbuilder v1.0.2
	github.com/stretchr/testify v1.7.0
)
