// Errors defines common errors.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

// objectError implements an error.
type objectError int

// Common errors.
const (
	ErrArg1NotContainedInParent = objectError(iota)
	ErrArg2NotContainedInParent
	ErrArgNotContainedInParent
	ErrArgNotAnIntegralVector
	ErrDecodingIntoNilObject
	ErrDecodingIntoExistingObject
	ErrDimensionsDoNotAgree
	ErrDimensionMustBeNonNegative
	ErrInvalidIndexRange
	ErrParentsDoNotAgree
	ErrSliceLengthNotEqualDimension
	ErrUnknownMonomialOrder
)

/////////////////////////////////////////////////////////////////////////
// objectError functions
/////////////////////////////////////////////////////////////////////////

// Error returns the error message.
func (e objectError) Error() string {
	switch e {
	case ErrArg1NotContainedInParent:
		return "argument 1 is not contained in the given parent"
	case ErrArg2NotContainedInParent:
		return "argument 2 is not contained in the given parent"
	case ErrArgNotContainedInParent:
		return "argument is not contained in the given parent"
	case ErrArgNotAnIntegralVector:
		return "argument is not an integral vector"
	case ErrDecodingIntoNilObject:
		return "decoding into nil object"
	case ErrDecodingIntoExistingObject:
		return "decoding into existing object"
	case ErrDimensionsDoNotAgree:
		return "arguments must have the same dimension"
	case ErrDimensionMustBeNonNegative:
		return "dimension must be a non-negative integer"
	case ErrInvalidIndexRange:
		return "index is out of range"
	case ErrParentsDoNotAgree:
		return "arguments must have the same parent"
	case ErrSliceLengthNotEqualDimension:
		return "the length of the slice does not match the dimension of the parent"
	case ErrUnknownMonomialOrder:
		return "unknown monomial order"
	default:
		return "unknown error"
	}
}

// Is return true iff target is equal to e.
func (e objectError) Is(target error) bool {
	ee, ok := target.(objectError)
	return ok && ee == e
}
