// Slice contains utilities for working with slices of integer vectors.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

import (
	"bitbucket.org/pcasmath/object"
	"bitbucket.org/pcasmath/slice"
)

// Slice implements the slice.Interface for a slice of Elements.
type Slice []*Element

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S Slice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S Slice) Entry(i int) object.Element {
	return S[i]
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S Slice) Slice(k int, m int) slice.Interface {
	return S[k:m]
}

// Hash returns a hash value for this slice.
func (S Slice) Hash() uint32 {
	n := len(S)
	if n == 0 {
		return 0
	}
	h := S[0].Hash()
	for i := 1; i < n; i++ {
		h = object.CombineHash(h, S[i].Hash())
	}
	return h
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// CopySlice returns a copy of the slice S. The capacity will be preserved.
func CopySlice(S []*Element) []*Element {
	T := make([]*Element, len(S), cap(S))
	copy(T, S)
	return T
}

// NCopiesToSlice returns a slice of length N containing the element a.
func NCopiesToSlice(a *Element, N int) []*Element {
	if N < 0 {
		N = 0
	}
	T := make([]*Element, 0, N)
	for i := 0; i < N; i++ {
		T = append(T, a)
	}
	return T

}
